package utils

import "github.com/gofiber/fiber/v2"

func GetInstanceUrl(c *fiber.Ctx) string {
	proto := "https://"
	if !Config.Secure {
		proto = "http://"
	}
	return proto + c.Hostname()
}
